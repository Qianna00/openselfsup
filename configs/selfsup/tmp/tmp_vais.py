_base_ = '../../base.py'

model = dict(
    type='TMP',
    pretrained='torchvision://resnet18',
    backbone=dict(
        type='ResNet',
        depth=18,
        in_channels=3,
        out_indices=[4],
        norm_cfg=dict(type='BN'),
        norm_eval=False),
    neck=dict(
        type='BNNeck',
        feat_dim=512,
        norm_cfg=dict(type='BN1d'),
        with_bias=False,
        with_avg_pool=True,
        avgpool=dict(type='AvgPoolNeck')),
    head=dict(type='AnotherSCLHead', temperature=0.05))

# dataset settings
data_source_cfg = dict(type='VAIS')
data_train_list = '/root/data/zq/data/VAIS/train.txt'
data_test_list = '/root/data/zq/data/VAIS/test.txt'
dataset_type = 'TMPDataset'
img_norm_cfg = dict(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
train_pipeline = [
    dict(type='RandomResizedCrop', size=224, scale=(0.2, 1.)),
    dict(type='RandomGrayscale', p=0.2),
    dict(
        type='ColorJitter',
        brightness=0.4,
        contrast=0.4,
        saturation=0.4,
        hue=0.4),
    dict(type='RandomHorizontalFlip'),
    dict(type='ToTensor'),
    dict(type='Normalize', **img_norm_cfg)
]
test_pipeline = [
    dict(type='Resize', size=(244, 244), interpolation=3),
    dict(type='ToTensor'),
    dict(
        type='Normalize', **img_norm_cfg)
]
data = dict(
    imgs_per_gpu=32,  # total 32*8=256
    workers_per_gpu=8,
    drop_last=True,
    train=dict(
        type=dataset_type,
        data_source=dict(
            list_file=data_train_list, **data_source_cfg),
        pipeline=train_pipeline),)
"""val=dict(
        type='ClassificationDataset',
        data_source=dict(list_file=data_test_list, **data_source_cfg),
        pipeline=test_pipeline))"""


custom_hooks = [
    dict(
        type='LabelGenerationHook',
        extractor=dict(
            dataset=dict(
                type='ExtractDataset',
                data_source=dict(list_file=data_train_list, **data_source_cfg),
                pipeline=test_pipeline),
            samples_per_gpu=32,
            workers_per_gpu=4),
        label_generator=dict(
            type='SelfPacedGenerator', eps=[0.75], min_samples=4, k1=30, k2=6, n_cluster=6),
        interval=1),
]

optimizer = dict(type='SGD', lr=0.05, weight_decay=5e-4, momentum=0.9)
# learning policy
lr_config = dict(
    policy='CosineAnnealing',
    min_lr=0.,
    warmup='linear',
    warmup_iters=5,
    warmup_ratio=0.0001,  # cannot be 0
    warmup_by_epoch=True)
total_epochs = 50
checkpoint_config = dict(interval=2)
log_config = dict(
    interval=2,
    hooks=[
        dict(type='TextLoggerHook'),
        dict(type='TensorboardLoggerHook')
    ])
