import torch
import os
import pickle as pkl
from collections import OrderedDict
import sys


if __name__ == "__main__":
    input = sys.argv[1]

    obj = torch.load(input, map_location="cpu")
    obj = obj["state_dict"]

    newmodel = {}
    for key in obj.keys():
        if key.startswith('backbone'):
            newkey = key[9:]
            newmodel[newkey] = obj[key]

    assert sys.argv[2].endswith('.pth')
    torch.save(newmodel, sys.argv[2])
