from .builder import build_label_generator, LABEL_GENERATORS
from .self_paced import SelfPacedGenerator