from ..registry import DATASOURCES
from .image_list import ImageList
from PIL import Image


@DATASOURCES.register_module
class Marvel(object):

    def __init__(self, list_file):
        with open(list_file, 'r') as f:
            lines = f.readlines()
        self.data_items = []
        self.fns = []
        self.labels = []
        for l in lines:
            fn = l.strip().split(',')[-1]
            label = int(l.split(',')[2]) - 1
            self.fns.append(fn)
            self.labels.append(label)
            self.data_items.append((fn, label))

        self.classes = ['Container Ship', 'Bulk Carrier', 'Passengers Ship', 'Ro-ro/passenger Ship',
                        'Ro-ro Cargo', 'Tug', 'Vehicles Carrier', 'Reefer', 'Yacht', 'Sailing Vessel',
                        'Heavy Load Carrier', 'Wood Chips Carrier', 'Livestock Carrier', 'Fire Fighting Vessel',
                        'Patrol Vessel', 'Platform', 'Standby Safety Vessel', 'Combat Vessel',
                        'Training Ship', 'Icebreaker', 'Replenishment Vessel', 'Tankers', 'Fishing Vessels',
                        'Supply Vessels', 'Carrier/Floating', 'Dredgers']

    def get_length(self):
        return len(self.fns)

    def get_sample(self, idx):
        img = Image.open(self.fns[idx])
        img = img.convert('RGB')
        # label = self.labels[idx]
        return img
