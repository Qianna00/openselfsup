import torch
from PIL import Image
from .registry import DATASETS, PIPELINES
from .base import BaseDataset
from openselfsup.utils import print_log, build_from_cfg
from torchvision.transforms import Compose
import random
from .contrastive import ContrastiveDataset


@DATASETS.register_module
class TMPDataset(ContrastiveDataset):
    """Dataset for rotation prediction 
    """

    def __init__(self, data_source, pipeline):
        super(TMPDataset, self).__init__(data_source, pipeline)
        self.data_items = self.data_source.data_items

    def __getitem__(self, idx):
        # img, label = self.data_source.get_sample(idx)
        img, label = self.data_items[idx]
        img = Image.open(img)
        img = img.convert('RGB')
        assert isinstance(img, Image.Image), \
            'The output from the data source must be an Image, got: {}. \
            Please ensure that the list file does not contain labels.'.format(
            type(img))
        img1 = self.pipeline(img)
        img2 = self.pipeline(img)
        img_cat = torch.cat((img1.unsqueeze(0), img2.unsqueeze(0)), dim=0)
        return dict(img=img_cat, label=label)

    def evaluate(self, scores, keyword, logger=None):
        raise NotImplemented

    def update_labels(self, labels):
        assert len(labels) == len(self)

        # update self.img_items
        data_items = []
        for i in range(len(self)):
            data_items.append(
                (self.data_items[i][0], labels[i]))
        self.data_items = data_items
